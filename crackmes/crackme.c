#include <stdio.h>

int test(int t, int p)
{
  return t == p;
}

int shift(int c)
{
  for(int i = 0; i <= c; i++)
    {
      printf("%d\n", i);
    }
}

int main()
{
  int p = 0x1234;
  if(test(1234, p) == 0x11)
    {
      printf("%s\n", "OK");
    }
  else
    {
      printf("%s\n", "FAIL");
    }
  return 0;
}
