#include <stdio.h>

int main(int argc, char **argv)
{
	int a = 1 << 31;
	printf("shifting into sign: %d\n", a);

	int b = -1 << 2;
	printf("shifting negative: %d\n", b);

	int c = 1 << 32;
	printf("shifting large offset: %d\n", c);
}
