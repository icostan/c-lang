#include <stdio.h>
#include "functions.h"

int add(int, int);

int op(int (*o)(int, int), int x, int y)
{
	return o(x, y);
}

int mul(int x, int y)
{
	return x * y;
}

int main(int argc, char **argv)
{
	int total = add(2, 3);
	printf("add: %d\n", total);
	printf("add macro: %d\n", ADD(2, 3));

	/* typedef declaration */
	operation o;
	o = add;
	printf("add typedef: %d\n", (*o)(2, 3));

	/* declaration of function pointer */
	int (*f)(int, int);
	f = mul;
	printf("mul pointer: %d\n", op(f, 2, 3));
	f = &add;
	printf("add pointer: %d\n", op(f, 2, 3));
}

int add(int x, int y)
{
	return x + y;
}
