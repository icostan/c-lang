#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int x = 6;
	int *px = &x;

	printf("x: %d\n", *px);
	printf("px: %p\n", (void *)px);

	int arr[10];
	arr[5] = 5;
	printf("5th: %d\n", arr[5]);

	int *ptr = malloc(sizeof(*ptr) * 20);
	ptr[10] = 10;
	printf("10th: %d\n", *(ptr + 10));
	printf("overflow: %d\n", *(ptr + 22));

	size_t size = 10;
	ptr = calloc(size, sizeof(int));
	ptr[0] = 9;
	printf("ptr: %d, %d\n", *ptr, *(ptr + 1));

	size++;
	ptr = realloc(ptr, sizeof(int) * size);
	free(ptr);
}
