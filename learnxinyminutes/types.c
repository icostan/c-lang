#include <stdio.h>

/* defines bool type */
typedef enum { false, true } bool;
typedef char *String;

struct point {
	int x;
	int y;
};

struct point origin = { 0, 0 };

struct rectangle {
	struct point p;
	int width;
	int height;
} rect;
typedef struct rectangle R;

int area(R *r);

int main(int argc, char **argv)
{
	bool b = true;
	printf("true: %d\n", b);

	String s = "world";
	printf("Hello: %s\n", s);

	bool same = sizeof(struct rectangle) == sizeof(int) + sizeof(int);
	printf("same?: %d\n", same);

	R *rectp = &rect;
	rectp->width = 20;
	rect.height = 10;
	rect.p = origin;
	printf("area: %d, p: (%d, %d)\n", area(rectp), (*rectp).p.x, rectp->p.y);
}

int area(struct rectangle *r)
{
	return r->width * r->height;
}
