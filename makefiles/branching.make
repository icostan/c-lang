sqrt_0  = 0.000000
sqrt_1  = 1.000000
sqrt_2  = 1.414214
sqrt_3  = 1.732051

square_0  = 1
square_1  = 2
square_2  = 4
square_3  = 9

# expansion of both sides, the left side and right side
result           := $(op)($(n)) = $($(op)_$(n))
result$(verbose) := $($(op)_$(n))

output :
	@printf "result: $(result)\n"
