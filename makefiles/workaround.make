USE_FOO = 0

EXTRA_FLAGS_0 = -Wbar
EXTRA_FLAGS_1 = -ffoo -lfoo

# expansion of the right side
EXTRA_FLAGS = $(EXTRA_FLAGS_$(USE_FOO))

output :
	@printf "EXTRA_FLAGS: $(EXTRA_FLAGS)\n"
