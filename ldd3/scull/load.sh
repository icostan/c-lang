#!/usr/bin/env sh

module="scull"
device="scull"
mode="664"

# invoke insmod with all arguments we got
# and use a pathname, as newer modutils don't look in . by default
echo 'Installing module...'
insmod ./$module.ko $* || exit 1

# major=$(awk "\\$2=  =\"$module\" {print \\$1}" /proc/devices)
major=6

echo 'Creating /dev nodes...'
mknod /dev/${device}0 c $major 0
mknod /dev/${device}1 c $major 1
mknod /dev/${device}2 c $major 2
mknod /dev/${device}3 c $major 3
mknod /dev/${device}pipe c $major 6

# give appropriate group/permissions, and change the group.
# Not all distributions have staff, some have "wheel" instead.
group="users"
# grep -q '^staff:' /etc/group || group="wheel"

echo 'Change permissions...'
chgrp $group /dev/${device}*
chmod $mode  /dev/${device}*
