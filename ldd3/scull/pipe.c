#include <asm-generic/errno-base.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/wait.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/poll.h>

#include "scull.h"

static int scull_pipe_minor = 6;
static int scull_pipe_buffer = 4000; /* buffer size */

struct scull_pipe {
	wait_queue_head_t inq, outq; /* read and write queues */
	char *buffer, *end; /* begin of buf, end of buf */
	int buffersize; /* used in pointer arithmetic */
	char *rp, *wp; /* where to read, where to write */
	int nreaders, nwriters; /* number of openings for r/w */
	struct fasync_struct *async_queue; /* asynchronous readers */
	struct mutex mutex; /* mutual exclusion */
	struct cdev cdev; /* Char device structure */
};

static struct scull_pipe *scull_pipe_device;
static dev_t devt;

static int scull_pipe_fasync(int fd, struct file *filp, int mode)
{
	struct scull_pipe *dev = filp->private_data;

	return fasync_helper(fd, filp, mode, &dev->async_queue);
}

static int scull_pipe_open(struct inode *inode, struct file *filp)
{
	struct scull_pipe *dev;

	dev = container_of(inode->i_cdev, struct scull_pipe, cdev);
	filp->private_data = dev;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if (!dev->buffer) {
		/* allocate the buffer */
		dev->buffer = kmalloc(scull_pipe_buffer, GFP_KERNEL);
		if (!dev->buffer) {
			mutex_unlock(&dev->mutex);
			return -ENOMEM;
		}
	}
	dev->buffersize = scull_pipe_buffer;
	dev->end = dev->buffer + dev->buffersize;
	dev->rp = dev->wp = dev->buffer; /* rd and wr from the beginning */

	/* use f_mode,not  f_flags: it's cleaner (fs/open.c tells why) */
	if (filp->f_mode & FMODE_READ)
		dev->nreaders++;
	if (filp->f_mode & FMODE_WRITE)
		dev->nwriters++;
	mutex_unlock(&dev->mutex);

	return nonseekable_open(inode, filp);
}

static int scull_pipe_release(struct inode *inode, struct file *filp)
{
	struct scull_pipe *dev = filp->private_data;

	/* remove this filp from the asynchronously notified filp's */
	scull_pipe_fasync(-1, filp, 0);

	mutex_lock(&dev->mutex);
	if (filp->f_mode & FMODE_READ)
		dev->nreaders--;
	if (filp->f_mode & FMODE_WRITE)
		dev->nwriters--;
	if (dev->nreaders + dev->nwriters == 0) {
		kfree(dev->buffer);
		dev->buffer =
			NULL; /* the other fields are not checked on open */
	}
	mutex_unlock(&dev->mutex);

	return 0;
}

static ssize_t scull_pipe_read(struct file *filp, char __user *buf,
			       size_t count, loff_t *f_pos)
{
	struct scull_pipe *dev = filp->private_data;

	printk(KERN_NOTICE "Scull pipe read...\n");

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	while (dev->rp == dev->wp) { /* nothing to read */
		mutex_unlock(&dev->mutex);
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		PDEBUG("\"%s\" reading: going to sleep\n", current->comm);
		if (wait_event_interruptible(dev->inq, (dev->rp != dev->wp)))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		/* otherwise loop, but first reacquire the lock */
		if (mutex_lock_interruptible(&dev->mutex))
			return -ERESTARTSYS;
	}
	/* ok, data is there, return something */
	if (dev->wp > dev->rp)
		count = min(count, (size_t)(dev->wp - dev->rp));
	else /* the write pointer has wrapped, return data up to dev->end */
		count = min(count, (size_t)(dev->end - dev->rp));
	if (copy_to_user(buf, dev->rp, count)) {
		mutex_unlock(&dev->mutex);
		return -EFAULT;
	}
	dev->rp += count;
	if (dev->rp == dev->end)
		dev->rp = dev->buffer; /* wrapped */
	mutex_unlock(&dev->mutex);

	/* finally, awake any writers and return */
	wake_up_interruptible(&dev->outq);
	PDEBUG("\"%s\" did read %li bytes\n", current->comm, (long)count);
	return count;
}

/* How much space is free? */
static int spacefree(struct scull_pipe *dev)
{
	if (dev->rp == dev->wp)
		return dev->buffersize - 1;
	return ((dev->rp + dev->buffersize - dev->wp) % dev->buffersize) - 1;
}

/* Wait for space for writing; caller must hold device semaphore.  On
 * error the semaphore will be released before returning. */
static int scull_getwritespace(struct scull_pipe *dev, struct file *filp)
{
	while (spacefree(dev) == 0) { /* full */
		DEFINE_WAIT(wait);

		mutex_unlock(&dev->mutex);
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		PDEBUG("\"%s\" writing: going to sleep\n", current->comm);
		prepare_to_wait(&dev->outq, &wait, TASK_INTERRUPTIBLE);
		if (spacefree(dev) == 0)
			schedule();
		finish_wait(&dev->outq, &wait);
		if (signal_pending(current))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		if (mutex_lock_interruptible(&dev->mutex))
			return -ERESTARTSYS;
	}
	return 0;
}

static unsigned int scull_pipe_poll(struct file *filp, poll_table *wait)
{
	struct scull_pipe *dev = filp->private_data;
	unsigned int mask = 0;

	printk(KERN_NOTICE "Scull pipe poll...\n");

	/*
     * The buffer is circular; it is considered full
     * if "wp" is right behind "rp" and empty if the
     * two are equal.
     */
	mutex_lock_interruptible(&dev->mutex);
	poll_wait(filp, &dev->inq, wait);
	poll_wait(filp, &dev->outq, wait);
	if (dev->rp != dev->wp)
		mask |= POLLIN | POLLRDNORM; /* readable */
	if (spacefree(dev))
		mask |= POLLOUT | POLLWRNORM; /* writable */
	mutex_unlock(&dev->mutex);
	return mask;
}

static ssize_t scull_pipe_write(struct file *filp, const char __user *buf,
				size_t count, loff_t *f_pos)
{
	struct scull_pipe *dev = filp->private_data;
	int result;

	printk(KERN_NOTICE "Scull pipe write...\n");

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	/* Make sure there's space to write */
	result = scull_getwritespace(dev, filp);
	if (result)
		return result; /* scull_getwritespace called up(&dev->sem) */

	/* ok, space is there, accept something */
	count = min(count, (size_t)spacefree(dev));
	if (dev->wp >= dev->rp)
		/* to end-of-buf */
		count = min(count, (size_t)(dev->end - dev->wp));
	else /* the write pointer has wrapped, fill up to rp-1 */
		count = min(count, (size_t)(dev->rp - dev->wp - 1));
	PDEBUG("Going to accept %li bytes to %p from %p\n", (long)count,
	       dev->wp, buf);
	if (copy_from_user(dev->wp, buf, count)) {
		mutex_unlock(&dev->mutex);
		return -EFAULT;
	}
	dev->wp += count;
	if (dev->wp == dev->end)
		dev->wp = dev->buffer; /* wrapped */
	mutex_unlock(&dev->mutex);

	/* finally, awake any reader blocked in read(  ) and select(  ) */
	wake_up_interruptible(&dev->inq);

	/* and signal asynchronous readers, explained late in chapter 5 */
	if (dev->async_queue)
		kill_fasync(&dev->async_queue, SIGIO, POLL_IN);
	PDEBUG("\"%s\" did write %li bytes\n", current->comm, (long)count);
	return count;
}

struct file_operations scull_pipe_fops = {
	.owner = THIS_MODULE,
	.llseek = no_llseek,
	.read = scull_pipe_read,
	.write = scull_pipe_write,
	.poll = scull_pipe_poll,
	/* .unlocked_ioctl = scull_ioctl, */
	.open = scull_pipe_open,
	.release = scull_pipe_release,
	.fasync = scull_pipe_fasync,
};

void scull_pipe_exit(void)
{
	/* free structs */
	if (scull_pipe_device) {
		cdev_del(&scull_pipe_device->cdev);
		kfree(scull_pipe_device->buffer);
		kfree(scull_pipe_device);
	}

	/* unregister chrdev */
	unregister_chrdev_region(devt, 1);

	printk(KERN_ALERT "Scull pipe exit.\n");
}

int scull_pipe_init(void)
{
	int result;
	printk(KERN_ALERT "Scull pipe init.\n");

	devt = MKDEV(scull_major, scull_pipe_minor);

	printk(KERN_NOTICE "Scull pipe registering device...\n");
	result = register_chrdev_region(devt, 1, "scullpipe");
	if (result < 0) {
		printk(KERN_WARNING "scullpipe: register, %d\n", result);
		goto fail;
	}

	printk(KERN_NOTICE "Scull pipe allocating memory...\n");
	scull_pipe_device = kmalloc(sizeof(struct scull_pipe), GFP_KERNEL);
	if (scull_pipe_device == NULL) {
		result = -ENOMEM;
		goto fail;
	}
	memset(scull_pipe_device, 0, sizeof(struct scull_pipe));

	printk(KERN_NOTICE "Scull pipe init queues/mutex...\n");
	init_waitqueue_head(&scull_pipe_device->inq);
	init_waitqueue_head(&scull_pipe_device->outq);
	mutex_init(&scull_pipe_device->mutex);

	printk(KERN_NOTICE "Scull pipe init cdev...\n");
	scull_init_cdev(&scull_pipe_device->cdev, &scull_pipe_fops, devt);

	return 0;

fail:
	scull_pipe_exit();
	return result;
}
