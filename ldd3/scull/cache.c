#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/mutex.h>
#include <linux/ioctl.h>
#include <linux/capability.h>

#include "scull.h"

static char *name = "scull";
static int scull_minor = 0;
static int scull_nr_devs = 4;
static int scull_quantum = SCULL_QUANTUM;
static int scull_qset = SCULL_QSET;
static dev_t devt;

struct scull_qset {
	void **data;
	struct scull_qset *next;
};

struct scull_dev {
	struct scull_qset *data; /* Pointer to first quantum set */
	int quantum; /* the current quantum size */
	int qset; /* the current array size */
	unsigned long size; /* amount of data stored here */
	unsigned int access_key; /* used by sculluid and scullpriv */
	struct mutex mutex; /* mutual exclusion */
	struct cdev cdev; /* Char device structure      */
};

struct scull_dev *scull_devices; /* allocated in scull_init_module */

/* #ifdef SCULL_DEBUG /\* use proc only if debugging *\/ */

/* The scullmem proc implementation. */
int scull_read_procmem(struct seq_file *s, void *v)
{
	int i, j;
	int limit = s->size - 80; /* Don't print more characters than this. */

	for (i = 0; i < scull_nr_devs && s->count <= limit; i++) {
		struct scull_dev *d = &scull_devices[i];
		struct scull_qset *qs = d->data;

		if (mutex_lock_interruptible(&d->mutex))
			return -ERESTARTSYS;

		seq_printf(s, "\nDevice %i: qset %i, q %i, sz %li\n", i,
			   d->qset, d->quantum, d->size);
		for (; qs && s->count <= limit;
		     qs = qs->next) { /* Scan the list. */
			seq_printf(s, "  item at %p, qset at %p\n", qs,
				   qs->data);
			if (qs->data &&
			    !qs->next) /* Dump only the last item. */
				for (j = 0; j < d->qset; j++) {
					if (qs->data[j])
						seq_printf(s, "    % 4i: %8p\n",
							   j, qs->data[j]);
				}
		}

		mutex_unlock(&d->mutex);
	}
	return 0;
}

static int scullmem_proc_open(struct inode *inode, struct file *filp)
{
	return single_open(filp, scull_read_procmem, NULL);
}

struct file_operations scullmem_proc_ops = {
	.owner = THIS_MODULE,
	.open = scullmem_proc_open,
	.llseek = seq_lseek,
	.read = seq_read,
	.release = single_release,
};

/* #endif /\* SCULL_DEBUG *\/ */

long scull_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int err = 0, tmp;
	int retval = 0;

	/*
 * extract the type and number bitfields, and don't decode
 * wrong cmds: return ENOTTY (inappropriate ioctl) before access_ok(  )
 */
	if (_IOC_TYPE(cmd) != SCULL_IOC_MAGIC)
		return -ENOTTY;
	if (_IOC_NR(cmd) > SCULL_IOC_MAXNR)
		return -ENOTTY;

	/*
 * the direction is a bitmask, and VERIFY_WRITE catches R/W
 * transfers. `Type' is user-oriented, while
 * access_ok is kernel-oriented, so the concept of "read" and
 * "write" is reversed
 */
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok((void __user *)arg, _IOC_SIZE(cmd));
	else if (_IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok((void __user *)arg, _IOC_SIZE(cmd));
	if (err)
		return -EFAULT;

	switch (cmd) {
	case SCULL_IOCRESET:
		scull_quantum = SCULL_QUANTUM;
		scull_qset = SCULL_QSET;
		break;

	case SCULL_IOCSQUANTUM: /* Set: arg points to the value */
		if (!capable(CAP_SYS_ADMIN))
			return -EPERM;
		retval = get_user(scull_quantum, (int __user *)arg);
		break;

	case SCULL_IOCTQUANTUM: /* Tell: arg is the value */
		if (!capable(CAP_SYS_ADMIN))
			return -EPERM;
		scull_quantum = arg;
		break;

	case SCULL_IOCGQUANTUM: /* Get: arg is pointer to result */
		retval = put_user(scull_quantum, (int __user *)arg);
		break;

	case SCULL_IOCQQUANTUM: /* Query: return it (it's positive) */
		return scull_quantum;

	case SCULL_IOCXQUANTUM: /* eXchange: use arg as pointer */
		if (!capable(CAP_SYS_ADMIN))
			return -EPERM;
		tmp = scull_quantum;
		retval = get_user(scull_quantum, (int __user *)arg);
		if (retval == 0)
			retval = put_user(tmp, (int __user *)arg);
		break;

	case SCULL_IOCHQUANTUM: /* sHift: like Tell + Query */
		if (!capable(CAP_SYS_ADMIN))
			return -EPERM;
		tmp = scull_quantum;
		scull_quantum = arg;
		return tmp;

	default: /* redundant, as cmd was checked against MAXNR */
		return -ENOTTY;
	}
	return retval;
}

loff_t scull_llseek(struct file *filp, loff_t offset, int whence)
{
	struct scull_dev *dev = filp->private_data;
	loff_t newpos;

	switch (whence) {
	case 0: /* SEEK_SET */
		newpos = offset;
		break;

	case 1: /* SEEK_CUR */
		newpos = filp->f_pos + offset;
		break;

	case 2: /* SEEK_END */
		newpos = dev->size + offset;
		break;

	default: /* can't happen */
		return -EINVAL;
	}
	if (newpos < 0)
		return -EINVAL;

	filp->f_pos = newpos;
	return newpos;
}

int scull_trim(struct scull_dev *dev)
{
	struct scull_qset *next, *dptr;
	int qset = dev->qset; /* "dev" is not-null */
	int i;

	for (dptr = dev->data; dptr; dptr = next) { /* all the list items */
		if (dptr->data) {
			for (i = 0; i < qset; i++)
				kfree(dptr->data[i]);
			kfree(dptr->data);
			dptr->data = NULL;
		}
		next = dptr->next;
		kfree(dptr);
	}
	dev->size = 0;
	dev->quantum = scull_quantum;
	dev->qset = scull_qset;
	dev->data = NULL;
	return 0;
}

int scull_open(struct inode *inode, struct file *filp)
{
	struct scull_dev *dev; /* device information */

	dev = container_of(inode->i_cdev, struct scull_dev, cdev);
	filp->private_data = dev; /* for other methods */

	/* now trim to 0 the length of the device if open was write-only */
	if ((filp->f_flags & O_ACCMODE) == O_WRONLY) {
		scull_trim(dev); /* ignore errors */
	}
	return 0; /* success */
}

/*
 * Follow the list
 */
struct scull_qset *scull_follow(struct scull_dev *dev, int n)
{
	struct scull_qset *qs = dev->data;

	/* Allocate first qset explicitly if need be */
	if (!qs) {
		qs = dev->data = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
		if (qs == NULL)
			return NULL; /* Never mind */
		memset(qs, 0, sizeof(struct scull_qset));
	}

	/* Then follow the list */
	while (n--) {
		if (!qs->next) {
			qs->next =
				kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
			if (qs->next == NULL)
				return NULL; /* Never mind */
			memset(qs->next, 0, sizeof(struct scull_qset));
		}
		qs = qs->next;
		continue;
	}
	return qs;
}

ssize_t scull_read(struct file *filp, char __user *buf, size_t count,
		   loff_t *f_pos)
{
	struct scull_dev *dev = filp->private_data;
	struct scull_qset *dptr; /* the first listitem */
	int quantum = dev->quantum, qset = dev->qset;
	int itemsize = quantum * qset; /* how many bytes in the listitem */
	int item, s_pos, q_pos, rest;
	int retval = 0;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if (*f_pos >= dev->size)
		goto out;
	if (*f_pos + count > dev->size)
		count = dev->size - *f_pos;

	/* find listitem, qset index, and offset in the quantum */
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;
	printk(KERN_NOTICE
	       "Scull read dev: item=%d, rest=%d, s_pos=%d, q_pos=%d.\n",
	       item, rest, s_pos, q_pos);

	/* follow the list up to the right position (defined elsewhere) */
	dptr = scull_follow(dev, item);

	if (dptr == NULL || !dptr->data || !dptr->data[s_pos])
		goto out; /* don't fill holes */

	/* read only up to the end of this quantum */
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	if (copy_to_user(buf, dptr->data[s_pos] + q_pos, count)) {
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	retval = count;

out:
	printk(KERN_ALERT "Scull read out: %d.\n", retval);
	mutex_unlock(&dev->mutex);
	return retval;
}

ssize_t scull_write(struct file *filp, const char __user *buf, size_t count,
		    loff_t *f_pos)
{
	struct scull_dev *dev = filp->private_data;
	struct scull_qset *dptr;
	int quantum = dev->quantum, qset = dev->qset;
	int itemsize = quantum * qset;
	int item, s_pos, q_pos, rest;
	int retval = -ENOMEM; /* value used in "goto out" statements */

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	/* find listitem, qset index and offset in the quantum */
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;
	printk(KERN_NOTICE
	       "Scull write dev: item=%d, rest=%d, s_pos=%d, q_pos=%d.\n",
	       item, rest, s_pos, q_pos);

	/* follow the list up to the right position */
	printk(KERN_NOTICE "Scull follow.\n");
	dptr = scull_follow(dev, item);
	if (dptr == NULL)
		goto out;
	if (!dptr->data) {
		printk(KERN_NOTICE "Scull kmalloc data.\n");
		dptr->data = kmalloc(qset * sizeof(char *), GFP_KERNEL);
		if (!dptr->data)
			goto out;
		memset(dptr->data, 0, qset * sizeof(char *));
	}
	if (!dptr->data[s_pos]) {
		printk(KERN_NOTICE "Scull kmalloc data[%d].\n", s_pos);
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if (!dptr->data[s_pos])
			goto out;
	}
	/* write only up to the end of this quantum */
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	printk(KERN_NOTICE "Scull copy from user.\n");
	if (copy_from_user(dptr->data[s_pos] + q_pos, buf, count)) {
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	retval = count;

	/* update the size */
	printk(KERN_NOTICE "Scull update the size.\n");
	if (dev->size < *f_pos)
		dev->size = *f_pos;

out:
	printk(KERN_ALERT "Scull write out: %d.\n", retval);
	mutex_unlock(&dev->mutex);
	return retval;
}

struct file_operations scull_fops = {
	.owner = THIS_MODULE,
	.llseek = scull_llseek,
	.read = scull_read,
	.write = scull_write,
	.unlocked_ioctl = scull_ioctl,
	.open = scull_open,
	.release = scull_release,
};

void scull_cache_exit(void)
{
	int i;

	/* 4. remove /proc entry */
	remove_proc_entry("scull", NULL);

	/* 2+3. deallocate and remove each devices */
	if (scull_devices) {
		for (i = 0; i < scull_nr_devs; i++) {
			scull_trim(scull_devices + i);
			cdev_del(&scull_devices[i].cdev);
		}
		kfree(scull_devices);
	}

	/* 1. unregister char driver region */
	unregister_chrdev_region(devt, scull_nr_devs);

	printk(KERN_ALERT "Scull cache exit.\n");
}

int scull_cache_init(void)
{
	int result, i;

	printk(KERN_ALERT "Scull cache init.\n");

	/* 1. register char device region */
	if (scull_major) {
		devt = MKDEV(scull_major, scull_minor);
		result = register_chrdev_region(devt, scull_nr_devs, name);
	} else {
		result = alloc_chrdev_region(&devt, scull_minor, scull_nr_devs,
					     name);
		scull_major = MAJOR(devt);
	}
	if (result < 0) {
		printk(KERN_WARNING "scull: can't get major %d\n", scull_major);
		return result;
	}

	/* 2. allocate the devices */
	scull_devices =
		kmalloc(scull_nr_devs * sizeof(struct scull_dev), GFP_KERNEL);
	if (!scull_devices) {
		result = -ENOMEM;
		goto fail; /* Make this more graceful */
	}
	memset(scull_devices, 0, scull_nr_devs * sizeof(struct scull_dev));

	/* 3. initialize each device */
	for (i = 0; i < scull_nr_devs; i++) {
		scull_devices[i].quantum = scull_quantum;
		scull_devices[i].qset = scull_qset;
		mutex_init(&scull_devices[i].mutex);

		devt = MKDEV(scull_major, i);
		scull_init_cdev(&scull_devices[i].cdev, &scull_fops, devt);
	}

	/* 4. create /proc entry */
	proc_create_data("scull", 0, NULL, &scullmem_proc_ops, NULL);

	return 0;

fail:
	scull_cache_exit();
	return result;
}
