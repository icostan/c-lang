#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>

#include "scull.h"

int scull_major = 6;

int scull_release(struct inode *inode, struct file *filp)
{
	return 0;
}

void scull_init_cdev(struct cdev *cdev, struct file_operations *fops,
		     dev_t devt)
{
	int err;
	cdev_init(cdev, fops);
	cdev->owner = THIS_MODULE;
	cdev->ops = fops;

	err = cdev_add(cdev, devt, 1);
	if (err)
		printk(KERN_NOTICE "Error %d adding cdev", err);
}

static int scull_init(void)
{
	printk(KERN_ALERT "Scull init.\n");

	scull_cache_init();
	scull_pipe_init();

	return 0;
}

static void scull_exit(void)
{
	scull_cache_exit();
	scull_pipe_exit();

	printk(KERN_ALERT "Scull exit.\n");
}

module_init(scull_init);
module_exit(scull_exit);

MODULE_AUTHOR("Iulian Costan");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("scull char driver");
