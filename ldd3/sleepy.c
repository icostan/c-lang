#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/wait.h>
#include <linux/sched.h>

static DECLARE_WAIT_QUEUE_HEAD(wq);
static int flag = 0;

ssize_t sleepy_read(struct file *filp, char __user *buf, size_t count,
		    loff_t *pos)
{
	printk(KERN_DEBUG "process %i (%s) going to sleep\n", current->pid,
	       current->comm);
	wait_event_interruptible(wq, flag != 0);
	flag = 0;
	printk(KERN_DEBUG "awoken %i (%s)\n", current->pid, current->comm);
	return 0; /* EOF */
}

ssize_t sleepy_write(struct file *filp, const char __user *buf, size_t count,
		     loff_t *pos)
{
	printk(KERN_DEBUG "process %i (%s) awakening the readers...\n",
	       current->pid, current->comm);
	flag = 1;
	wake_up_interruptible(&wq);
	return count; /* succeed, to avoid retrial */
}

MODULE_AUTHOR("Iulian Costan");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("sleepy driver");
