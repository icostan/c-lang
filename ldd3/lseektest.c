#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

char buffer[4096];

int main(int argc, char *argv[])
{
	int count, fd;

	fd = open("/dev/scull0", O_RDONLY);
	count = read(fd, buffer, 4096);
	write(1, buffer, count);

	fd = open("/dev/scull0", O_RDONLY);
	lseek(fd, 5, SEEK_SET);
	count = read(fd, buffer, 4096);
	write(1, buffer, count);

	return 0;
}
