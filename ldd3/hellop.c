#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("Dual BSD/GPL");

static char *whom = "World";
static int howmany = 1;
module_param(howmany, int, S_IRUGO);
module_param(whom, charp, S_IRUGO);

static int hellop_init(void)
{
	int i;
	for (i = 0; i < howmany; i++) {
		printk(KERN_ALERT "Hello, %s\n", whom);
	}
	return 0;
}

static void hellop_exit(void)
{
	printk(KERN_ALERT "Goodbye, cruel %s\n", whom);
}

module_init(hellop_init);
module_exit(hellop_exit);
