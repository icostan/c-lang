#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>

ssize_t faulty_write(struct file *filp, const char *buf, size_t count,
		     loff_t *pos)
{
	/* make a simple fault by dereferencing a NULL pointer */
	*(int *)0 = 0;
	return 0;
}

ssize_t faulty_read(struct file *filp, char *buf, size_t count, loff_t *pos)
{
	int ret;
	char stack_buf[4];

	/* Let's try a buffer overflow  */
	memset(stack_buf, 0xff, 20);
	if (count > 4)
		count = 4; /* copy 4 bytes to the user */
	ret = copy_to_user(buf, stack_buf, count);
	if (!ret)
		return count;
	return ret;
}
