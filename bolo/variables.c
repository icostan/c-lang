#include <stdio.h>

typedef enum { false, true } bool;
typedef char *string;

void variables(void)
{
	/* char charvar = 'A'; */
	/* short shortvar = 7; */
	/* int intvar = 10; */
	/* long longvar = 15; */
	bool b = true;
	/* double doublevar = 3.1415; */
	/* float floatvar = 2.1718; */
	/* char carray[] = { 'a', 'b', 'c' }; */
	/* string s = "hello"; */
}

int main(int argc, char *argv[])
{
	__asm__ ("nop");
	printf("Hello World");
	__asm__ ("nop");
	string s = "hello";
	printf("%s", s);
	__asm__ ("nop");
	printf("%d", 10);
	__asm__ ("nop");

	return 0;
}
