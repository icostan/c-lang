#include <stdio.h>
#include <stdlib.h>

void func()
{
	puts("newfunc");
}

int funcret()
{
	int a = rand();
	return a;
}

void funcparams(int i)
{
	printf("funcparams: %d\n", i);
}

int main(int argc, char *argv[])
{
	func();
	int a = funcret();
	funcparams(a);
	return 0;
}
