void maths(void)
{
	int A = 10;
	int B = 15;

	__asm__("nop");
	int add = A + B;
	__asm__("nop");
	int sub = A - B;
	__asm__("nop");
	int mul = A * B;
	__asm__("nop");
	int div = A / B;

	__asm__("nop");

	__asm__("nop");
	int and = A & B;
	__asm__("nop");
	int or = A | B;
	__asm__("nop");
	int xor = A ^ B;
	__asm__("nop");
	int not = ~A;

	__asm__("nop");

	__asm__("nop");
	int rshift = A >> B;
	__asm__("nop");
	int lshift = A << B;
}
